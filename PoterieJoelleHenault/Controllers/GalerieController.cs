﻿using PoterieJoelleHenault.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoterieJoelleHenault.Controllers
{
    [Authorize]
    public class GalerieController : Controller
    {
        PoterieDB db = new PoterieDB();
        SqlConnection connection = new SqlConnection("Server=0cc11d26-f261-4639-a167-a1030128bcfa.sqlserver.sequelizer.com;Database=db0cc11d26f2614639a167a1030128bcfa;User ID=rkhzwjpqjmjfnghb;Password=zfvnC8diEzQWpToFZ5yLTEHiZUqdnHP32vQktiJPvjxBPWxLpYvJYNs3DmuQR856;");

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ListGalery(int page = 1, string filtre = "")
        {
            string filtreSql ="";
            if (filtre == "urne")
            {
                filtreSql = " WHERE Name LIKE '%Urne%'";
            }
            if (filtre == "unique")
            {
                filtreSql = " WHERE Name LIKE '%Unique%'";
            }
            List<Galerie> galerie = new List<Galerie>();
            SqlCommand command = new SqlCommand(
                "SELECT * FROM Galerie" + filtreSql
                ,connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Galerie item = new Galerie();
                item.ID = (int)reader["ID"];
                item.Name = (string)reader["Name"];
                item.ImageURL = (string)reader["ImageURL"];
                item.Description = (string)reader["Description"];
                item.Price = (int)reader["Price"];
                galerie.Add(item);
            }
            ViewBag.Page = page;
            ViewBag.Filtre = filtre;
            ViewBag.Max = galerie.Count;

            return View(galerie.Skip((page-1)*10).Take(10).ToList());
        }

        public ActionResult List()
        {
            List<Galerie> galerie = new List<Galerie>();
            connection.Open();
            SqlCommand command = new SqlCommand(
                "SELECT * FROM Galerie"
                , connection);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Galerie item = new Galerie();
                item.ID = (int)reader["ID"];
                item.Name = (string)reader["Name"];
                item.ImageURL = (string)reader["ImageURL"];
                item.Description = (string)reader["Description"];
                item.Price = (int)reader["Price"];
                galerie.Add(item);
            }
            return View(galerie);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Galerie model)
        {
            if (ModelState.IsValid)
            {
                List<Galerie> galerie = new List<Galerie>();
                connection.Open();
                SqlCommand command = new SqlCommand(
                    "INSERT INTO Galerie VALUES (@Name,@imageUrl,@description,@Price)"
                    , connection);

                SqlParameter name = new SqlParameter();
                name.ParameterName = "@Name";
                name.Value = model.Name;
                command.Parameters.Add(name);

                SqlParameter imageUrl = new SqlParameter();
                imageUrl.ParameterName = "@imageUrl";
                imageUrl.Value = model.ImageURL;
                command.Parameters.Add(imageUrl);

                SqlParameter description = new SqlParameter();
                description.ParameterName = "@description";
                description.Value = model.Description;
                command.Parameters.Add(description);

                SqlParameter Price = new SqlParameter();
                Price.ParameterName = "@Price";
                Price.Value = model.Price;
                command.Parameters.Add(Price);

                command.ExecuteNonQuery();
            }
            return View();
        }

        public ActionResult Update(int id)
        {
            connection.Open();
            SqlCommand command = new SqlCommand(
                "SELECT * FROM Galerie WHERE ID = @ID"
                , connection);

            SqlParameter ID = new SqlParameter();
            ID.ParameterName = "@ID";
            ID.Value = id;
            command.Parameters.Add(ID);

            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            Galerie item = new Galerie();
            item.ID = (int)reader["ID"];
            item.Name = (string)reader["Name"];
            item.ImageURL = (string)reader["ImageURL"];
            item.Description = (string)reader["Description"];
            item.Price = (int)reader["Price"];
            return View(item);
        }

        [HttpPost]
        public ActionResult Update(Galerie model)
        {
            if (ModelState.IsValid)
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                    "UPDATE Galerie SET Name = @Name, ImageURL = @imageUrl,Description = @description,Price = @Price WHERE ID = @ID"
                    , connection);

                SqlParameter name = new SqlParameter();
                name.ParameterName = "@Name";
                name.Value = model.Name;
                command.Parameters.Add(name);

                SqlParameter imageUrl = new SqlParameter();
                imageUrl.ParameterName = "@imageUrl";
                imageUrl.Value = model.ImageURL;
                command.Parameters.Add(imageUrl);

                SqlParameter description = new SqlParameter();
                description.ParameterName = "@description";
                description.Value = model.Description;
                command.Parameters.Add(description);

                SqlParameter Price = new SqlParameter();
                Price.ParameterName = "@Price";
                Price.Value = model.Price;
                command.Parameters.Add(Price);

                SqlParameter ID = new SqlParameter();
                ID.ParameterName = "@ID";
                ID.Value = model.ID;
                command.Parameters.Add(ID);

                command.ExecuteNonQuery();
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            connection.Open();
            SqlCommand command = new SqlCommand(
                "SELECT * FROM Galerie WHERE ID = @ID"
                , connection);

            SqlParameter ID = new SqlParameter();
            ID.ParameterName = "@ID";
            ID.Value = id;
            command.Parameters.Add(ID);

            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            Galerie item = new Galerie();
            item.ID = (int)reader["ID"];
            item.Name = (string)reader["Name"];
            item.ImageURL = (string)reader["ImageURL"];
            item.Description = (string)reader["Description"];
            item.Price = (int)reader["Price"];
            return View(item);
        }

        [HttpPost]
        public ActionResult Delete(Galerie model)
        {
            if (ModelState.IsValid)
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                    "DELETE FROM Galerie WHERE ID = @ID"
                    , connection);

                SqlParameter ID = new SqlParameter();
                ID.ParameterName = "@ID";
                ID.Value = model.ID;
                command.Parameters.Add(ID);

                command.ExecuteNonQuery();
            }
            return RedirectToAction("List","Galerie");
        }
    }
}
