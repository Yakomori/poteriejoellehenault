﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PoterieJoelleHenault.Models;

namespace PoterieJoelleHenault.Controllers
{

    [Authorize]
    public class TextesController : Controller
    {
        private PoterieDB db = new PoterieDB();

        public ActionResult Index()
        {
            return View(db.Textes.ToList());
        }

        public ActionResult Details(int id = 0)
        {
            Textes textes = db.Textes.Find(id);
            if (textes == null)
            {
                return HttpNotFound();
            }
            return View(textes);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Textes textes)
        {
            if (ModelState.IsValid)
            {
                db.Textes.Add(textes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(textes);
        }

        public ActionResult Edit(int id = 0)
        {
            Textes textes = db.Textes.Find(id);
            if (textes == null)
            {
                return HttpNotFound();
            }
            return View(textes);
        }

        [HttpPost]
        public ActionResult Edit(Textes textes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(textes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(textes);
        }

        public ActionResult Delete(int id = 0)
        {
            Textes textes = db.Textes.Find(id);
            if (textes == null)
            {
                return HttpNotFound();
            }
            return View(textes);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Textes textes = db.Textes.Find(id);
            db.Textes.Remove(textes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}