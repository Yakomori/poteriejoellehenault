﻿using PoterieJoelleHenault.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PoterieJoelleHenault.Controllers
{
    public class AdminController : Controller
    {
        private const string UserName = "jhenault";
        private const string Password = "poterie321";

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(User model)
        {
            if (model.Username == UserName && model.Password == Password)
            {
                FormsAuthentication.SetAuthCookie(UserName, true);
                return RedirectToAction("Index","Admin");
            }
            return View();
        }

        public ActionResult Deco()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}
