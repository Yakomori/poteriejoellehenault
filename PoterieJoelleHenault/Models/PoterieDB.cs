﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PoterieJoelleHenault.Models
{
    public class PoterieDB : DbContext
    {
        public DbSet<Galerie> Galerie { get; set; }
        public DbSet<Textes> Textes { get; set; }
    }
}