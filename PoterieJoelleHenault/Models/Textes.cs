﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoterieJoelleHenault.Models
{
    public class Textes
    {
        public int ID { get; set; }
        public string Langue { get; set; }
        public string Page { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}